class Calculator:
	@staticmethod
	def validated_args(x, y):
		allowed_types = (int, float, complex)
		if not isinstance(x, allowed_types) and not isinstance(y, allowed_types):
			raise ValueError

	def add(self, x, y):
		self.validated_args(x, y)
		return x + y

	def multiple(self, x, y):
		self.validated_args(x, y)
		return x * y

	def sub(self, x, y):
		self.validated_args(x, y)
		return x - y

	def div(self, x, y):
		self.validated_args(x, y)
		return x / y

calculator = Calculator()
result = calculator.add(2, 2)
print(result)