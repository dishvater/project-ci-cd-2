def calc():
	operators = ["+", "-", "*", "/"]
	first_value = input("podaj pierwszą wartość")
	operator = input("podaj działanie")
	second_value = input("podaj drugą wartość")
	if not first_value.isdigit():
		return "first value is not a number"
	elif operator.strip() not in operators:
		return "unknown operator"
	elif not second_value.isdigit():
		return "second value is not a number"
	else:
		return eval(f'{first_value} {operator} {second_value}')

if __name__ == "__main__":
    calc()