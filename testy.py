import unittest

from calculator import Calculator

class CalculatorTests(unittest.TestCase):
	def setUp(self):
		self.calculator = Calculator()

	def test_add_method(self):
		result = self.calculator.add(4, 2)
		self.assertEqual(6, result)

	def test_sub_method(self):
		result = self.calculator.sub(4, 2)
		self.assertEqual(2, result)

	def test_sub_method_invalid_value(self):
		self.assertRaises(ValueError, self.calculator.sub, "four", "five")

	def test_multiple_method(self):
		result = self.calculator.multiple(4, 2)
		self.assertEqual(8, result)

	def test_multiple_method_invalid_value(self):
		self.assertRaises(ValueError, self.calculator.multiple, "four", "five")

	def test_div_method(self):
		result = self.calculator.div(4, 2)
		self.assertEqual(2, result)

	def test_div_first_zero_method(self):
		result = self.calculator.div(0, 2)
		self.assertEqual(0, result)

	def test_div_method_invalid_value(self):
		self.assertRaises(ValueError, self.calculator.div, "four", "five")

	def test_add_method_invalid_value(self):
		self.assertRaises(ValueError, self.calculator.add, "four", "five")

	def test_div_zero_method_invalid_value(self):
		self.assertRaises(ZeroDivisionError, self.calculator.div, 4, 0)

if __name__ == '__main__':
    unittest.main()